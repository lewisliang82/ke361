<?php
namespace Admin\Controller;

use Admin\Model\TagModel;
class TagController extends AdminController
{
    public function index(){
        
        $this->assign('_list',$this->listAll(D('Tag')));
        $this->display();
    }
    public function add(){
        if(IS_POST){
            $TagModel = new TagModel();
            if($TagModel->edit()){
                $this->success('添加成功',U('index'));
            }else {
                $this->error('添加过程遇到错误');
            }
        }else{
            $this->display();
        }
    }
}

?>